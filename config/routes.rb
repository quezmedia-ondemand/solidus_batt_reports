Spree::Core::Engine.routes.draw do

  match '/admin/reports/top_products' => 'admin/reports#top_products',  :via  => [:get, :post],
                                                                        :as   =>  'top_products_admin_reports'

  match '/admin/reports/top_customers' => 'admin/reports#top_customers',  :via  => [:get, :post],
                                                                        :as   =>  'top_customers_admin_reports'

  match '/admin/reports/inventory_reports' => 'admin/reports#inventory_reports',  :via  => [:get, :post],
                                                                        :as   =>  'inventory_reports_admin_reports'

  match '/admin/reports/rental_reports' => 'admin/reports#rental_reports',  :via  => [:get, :post],
                                                                        :as   =>  'rental_reports_admin_reports'
end
