Deface::Override.new(:virtual_path => "spree/admin/reports/sales_total",
                    :name => "add_actions_to_sales_total_report",
                    :insert_after => "erb[silent]:contains('content_for :page_actions')",
                    :partial => "spree/admin/reports/sales_total_actions",
                    :original => 'adebf7733615743e41a7959a4796a8da39fc5afe',
                    :disabled => false)
