require_dependency 'spree/admin/reports_controller'

module Spree
  module Admin
    ReportsController.class_eval do
      EXTRA_REPORTS ||= {}
      [:top_products, :top_customers, :inventory_reports, :rental_reports].each do |report|
        EXTRA_REPORTS[report] = { name: report.to_s.titleize }
        self.add_available_report! report
      end

      def basic_report_setup(with_customers = true)
        @reports = EXTRA_REPORTS
        @products = Spree::Product.all
        @taxons = Spree::Taxon.all
        if with_customers
          @customers = Order.pluck(:email).uniq.select{|e| e.present? }.map{ |e| [e, e] }
        end
        if defined?(MultiDomainExtension)
          @stores = Store.all
        end
      end

      def base_report_top_render(filename)
        respond_to do |format|
          format.html { render :template => "spree/admin/reports/top_base" }
          other_formats(format, filename)
        end
      end

      def top_products
        basic_report_setup
        @report = Spree::BattReport::TopReport::TopProducts.new(params, 4)
        base_report_top_render("top_products")
      end

      def top_customers
        basic_report_setup
        @report = Spree::BattReport::TopReport::TopCustomers.new(params, 4)
        base_report_top_render("top_customers")
      end

      def inventory_reports
        basic_report_setup(with_customers = false)
        @report = Spree::BattVariantReports::InventoryReport.new(params)
        base_report_top_render("inventory_reports")
      end

      def rental_reports
        basic_report_setup
        @report = Spree::BattReport::RentalReport.new(params)
        base_report_top_render("rental_reports")
      end

      alias_method :old_sales_total, :sales_total
      def sales_total
        old_sales_total
        @report = Spree::OrdersReport::SalesReport.new(@orders, @totals)
        respond_to do |format|
          format.html { render :template => "spree/admin/reports/sales_total" }
          other_formats(format, "sales_total")
        end
      end

      def other_formats(format_obj, filename)
        format_obj.csv do
          rows = @report.ruportdata.data.map(&:data)
          csv_result = CSV.generate do |csv|
            csv << rows.first.keys # Headers
            rows.each{ |r| csv << r.values }
          end
          send_data csv_result, type: :csv, filename: "#{filename} - #{Date.today.strftime}.csv"
        end
        format_obj.pdf do
          send_data WickedPdf.new.pdf_from_string(@report.ruportdata.to_html),
            filename: "#{filename} - #{Date.today.strftime}.pdf", disposition: 'attachment', type: :pdf
        end
      end
    end
  end
end
