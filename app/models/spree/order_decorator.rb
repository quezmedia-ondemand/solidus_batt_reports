module Spree
  Order.class_eval do
    has_many :taxons, through: :products

    # Although here might not be the best place,
    # These scopes will help Order Reports
    scope :by_product_id, ->(product_id){ includes(:products).where(spree_products: { id: product_id }) }
    scope :by_taxon_id, ->(taxon_id){ includes(:taxons).where(spree_taxons: { id: taxon_id }) }
    def self.by_sku(sku)
      variant_ids = Spree::Variant.where("sku ~* ?", sku).pluck(:id)
      includes(:variants).where(spree_variants: { id: variant_ids })
    end
  end
end
