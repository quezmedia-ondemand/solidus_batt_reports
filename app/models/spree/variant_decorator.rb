module Spree
  Variant.class_eval do
    has_many :taxons, through: :product
  end
end
