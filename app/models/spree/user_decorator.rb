Spree.user_class.class_eval do
  scope :with_orders, ->(){ includes(:orders).where(spree_orders: { email: nil }) }
end
