// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/backend/all.js'

$(function() {
	if($('input#product_id').length > 0) {
		$('select#advanced_reporting_product_id').val($('input#product_id').val());
	}
	if($('input#taxon_id').length > 0) {
		$('select#advanced_reporting_taxon_id').val($('input#taxon_id').val());
	}
	$('div#advanced_report_search form').submit(function() {
		$('div#advanced_report_search form').attr('action', $('select#report').val());
	});
  select_report_elem = $('select#report');
  if(select_report_elem.length > 0){
    update_report_dropdowns(select_report_elem.val());
    $('select#report').change(function() { update_report_dropdowns($(this).val()); });

    if(created_at_gt != '') {
      $('input#search_created_at_gt').val(created_at_gt);
    }
    if(created_at_lt != '') {
      $('input#search_created_at_lt').val(created_at_lt);
    }
  }
})

var update_report_dropdowns = function(value) {
	if(value.match(/\/count$/) || value.match(/\/top_products$/)) {
		$('select#advanced_reporting_product_id,select#advanced_reporting_taxon_id').val('');
		$('div#taxon_products').hide();
	} else {
		$('div#taxon_products').show();
	}
};
