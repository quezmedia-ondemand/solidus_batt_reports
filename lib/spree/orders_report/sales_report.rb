class Spree::OrdersReport::SalesReport < Spree::OrdersReport
  def name
    "Sales Orders Report"
  end

  def description
    "Sales Orders Report"
  end

  def initialize orders, totals
    super(orders)
    data = totals

    self.ruportdata = Table(%w[Currency item_total adjustment_total sales_total])
    data.inject({}) { |h, (k, v) | h[k] = v[:sales_total]; h }.sort { |a, b| a[1] <=> b [1] }.reverse[0..data.size+1].each do |k, v|
      ruportdata << { "Currency" => k, "item_total" => data[k][:item_total], "adjustment_total" => data[k][:adjustment_total], "sales_total" => data[k][:sales_total] }
    end
    ruportdata.replace_column("item_total") { |r| "$%0.2f" % r.item_total }
    ruportdata.replace_column("adjustment_total") { |r| "$%0.2f" % r.adjustment_total }
    ruportdata.replace_column("sales_total") { |r| "$%0.2f" % r.sales_total }
    ruportdata.rename_column("item_total", "Item Total")
    ruportdata.rename_column("adjustment_total", "Adjustment Total")
    ruportdata.rename_column("sales_total", "Sales Total")
  end
end
