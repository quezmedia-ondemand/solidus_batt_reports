module Spree
  class BattReport
    include Ruport
    attr_accessor :orders, :product_text, :date_text, :taxon_text,
                  :ruportdata, :data, :params, :taxon, :product,
                  :product_in_taxon, :unfiltered_params, :sku,
                  :sku_text, :customer, :customer_text

    def name
      "Base Everbatt Report"
    end

    def description
      "Base Everbatt Report"
    end

    def initialize params
      self.params = params
      self.data = {}
      self.ruportdata = {}
      self.unfiltered_params = params[:search].blank?? {} : params[:search].clone

      params[:search] ||= {}
      if params[:search][:completed_at_gt].blank?
        if (Order.count > 0) && Order.minimum(:completed_at)
          params[:search][:completed_at_gt] = Order.minimum(:completed_at).beginning_of_day
        end
      else
        params[:search][:completed_at_gt] = Time.zone.parse(params[:search][:completed_at_gt]).beginning_of_day rescue ""
      end
      if params[:search][:completed_at_lt].blank?
        if (Order.count > 0) && Order.maximum(:completed_at)
          params[:search][:completed_at_lt] = Order.maximum(:completed_at).end_of_day
        end
      else
        params[:search][:completed_at_lt] = Time.zone.parse(params[:search][:completed_at_lt]).end_of_day rescue ""
      end

      params[:search][:completed_at_not_null] = true
      params[:search][:state_not_eq] = 'canceled'

      self.product_in_taxon = true
      self.sku = false
      self.customer = false
      if params[:advanced_reporting]
        if params[:advanced_reporting][:taxon_id] && params[:advanced_reporting][:taxon_id] != ''
          self.taxon = Taxon.find(params[:advanced_reporting][:taxon_id])
        end
        if params[:advanced_reporting][:product_id] && params[:advanced_reporting][:product_id] != ''
          self.product = Product.find(params[:advanced_reporting][:product_id])
        end
        self.sku = params[:advanced_reporting][:sku]
        self.customer = params[:advanced_reporting][:customer_email]
      end

      if self.taxon && self.product && !self.product.taxons.include?(self.taxon)
        self.product_in_taxon = false
      end

      search = Order.search(params[:search])

      product_orders_ids, taxon_orders_ids, sku_orders_ids, customer_orders_ids = [], [], [], []

      if self.product
        self.product_text = "Product: #{self.product.name}<br />"
        product_orders_ids = Spree::Order.by_product_id(self.product.id).uniq.pluck(:id)
      end

      if self.taxon
        self.taxon_text = "Taxon: #{self.taxon.name}<br />"
        taxon_orders_ids = Spree::Order.by_taxon_id(self.taxon.id).uniq.pluck(:id)
      end

      if self.sku.present?
        self.sku_text = "Sku: #{self.sku}<br />"
        sku_orders_ids = Spree::Order.by_sku(self.sku).pluck(:id)
      end

      if self.customer
        self.customer_text = "Customer: #{self.customer}<br />"
        customer_orders_ids = Spree::Order.where(email: self.customer).uniq.pluck(:id)
      end

      filters_ids = (product_orders_ids + taxon_orders_ids + sku_orders_ids + customer_orders_ids).uniq
      self.orders = filters_ids.any?? search.result.where(id: filters_ids) : search.result(distint: true)

      # Above searchlogic date settings
      self.date_text = "Date Range:"
      if self.unfiltered_params
        if self.unfiltered_params[:completed_at_gt] != '' && self.unfiltered_params[:completed_at_lt] != ''
          self.date_text += " From #{self.unfiltered_params[:completed_at_gt]} to #{self.unfiltered_params[:completed_at_lt]}"
        elsif self.unfiltered_params[:completed_at_gt] != ''
          self.date_text += " After #{self.unfiltered_params[:completed_at_gt]}"
        elsif self.unfiltered_params[:completed_at_lt] != ''
          self.date_text += " Before #{self.unfiltered_params[:completed_at_lt]}"
        else
          self.date_text += " All"
        end
      else
        self.date_text += " All"
      end
    end

    def revenue(order)
      rev = order.item_total
      if !self.product.nil? && product_in_taxon
        rev = order.line_items.select { |li| li.product == self.product }.inject(0) { |a, b| a += b.quantity * b.price }
      elsif !self.taxon.nil?
        rev = order.line_items.select { |li| li.product && li.product.taxons.include?(self.taxon) }.inject(0) { |a, b| a += b.quantity * b.price }
      end
      self.product_in_taxon ? rev : 0
    end

    def units(order)
      units = order.line_items.sum(:quantity)
      if !self.product.nil? && product_in_taxon
        units = order.line_items.select { |li| li.product == self.product }.inject(0) { |a, b| a += b.quantity }
      elsif !self.taxon.nil?
        units = order.line_items.select { |li| li.product && li.product.taxons.include?(self.taxon) }.inject(0) { |a, b| a += b.quantity }
      end
      self.product_in_taxon ? units : 0
    end
  end
end
