module Spree
  class OrdersReport
    include Ruport
    attr_accessor :orders, :ruportdata, :data

    def name
      "Base Everbatt Orders Report"
    end

    def description
      "Base Everbatt Orders Report"
    end

    def initialize orders
      self.orders = orders
      self.ruportdata = {}
    end
  end
end
