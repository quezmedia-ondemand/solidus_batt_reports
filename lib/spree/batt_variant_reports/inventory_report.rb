class Spree::BattVariantReports::InventoryReport < Spree::BattVariantReports
  def initialize params, limit = nil
    super(params)

    variants_with_count_on_hand = Spree::StockItem.includes(:variant)
      .where(spree_variants: {id: variants.pluck(:id)})
      .group_by{ |stock_item| stock_item.variant }.map do |variant, stock_item_list|
        [variant, stock_item_list.first]
      end.to_h

    variants_with_count_on_hand.each do |variant, stock_item|
      data[variant.id] ||= {
        :name => variant.name,
        :sku => variant.sku,
        :product => variant.product.sku,
        :count_on_hand => stock_item.count_on_hand
      }
    end

    self.ruportdata = Table(%w[variant_name variant_sku product_sku count_on_hand])
    data.inject({}) { |h, (k, v) | h[k] = v[:count_on_hand]; h }.sort { |a, b| a[1] <=> b [1] }.reverse[0..(limit.present?? limit : data.size+1)].each do |k, v|
      ruportdata << { "variant_name" => data[k][:name], "variant_sku" => data[k][:sku], "product_sku" => data[k][:product], "count_on_hand" => data[k][:count_on_hand] }
    end
    ruportdata.rename_column("variant_name", "Variant Name")
    ruportdata.rename_column("variant_sku", "Variant Sku")
    ruportdata.rename_column("product_sku", "Product Sku")
    ruportdata.rename_column("count_on_hand", "Count On Hand")
  end
end
