require 'action_view'
include ActionView::Helpers::DateHelper

class Spree::BattReport::RentalReport < Spree::BattReport
  def name
    "Rental Reports"
  end

  def description
    "Rental Reports"
  end

  def initialize params, limit = nil
    super(params)

    orders.each do |order|
      order.line_items.rentable.each do |li|
        if !li.product.nil?
          data[li.product.id] ||= {
            :name => li.product.name.to_s,
            :rentable_at => I18n.l(li.created_at.to_date, format: :long),
            :rentable_until => I18n.l(li.renting_end_date, format: :long),
            :remaining_period => distance_of_time_in_words(li.created_at, li.renting_end_date),
            :remaining_period_in_seconds => ((li.renting_end_date - li.created_at.to_date) * 24 * 3600).to_i,
            :revenue => 0,
            :units => 0
          }
          data[li.product.id][:revenue] += li.quantity*li.price
          data[li.product.id][:units] += li.quantity
        end
      end
    end

    self.ruportdata = Table(%w[name rentable_at rentable_until remaining_period Units Revenue])
    data.inject({}) { |h, (k, v) | h[k] = v[:remaining_period_in_seconds]; h }.sort { |a, b| a[1] <=> b [1] }.reverse[0..(limit.present?? limit : data.size+1)].each do |k, v|
      ruportdata << { "name" => data[k][:name], "rentable_at" => data[k][:rentable_at], "remaining_period" => data[k][:remaining_period], "rentable_until" => data[k][:rentable_until], "Units" => data[k][:units], "Revenue" => data[k][:revenue] }
    end
    ruportdata.replace_column("Revenue") { |r| "$%0.2f" % r.Revenue }
    ruportdata.rename_column("name", "Product Name")
    ruportdata.rename_column("rentable_at", "Rented At")
    ruportdata.rename_column("rentable_until", "Rented Until")
    ruportdata.rename_column("remaining_period", "Remaining Renting Period")
  end
end
