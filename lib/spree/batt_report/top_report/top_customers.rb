class Spree::BattReport::TopReport::TopCustomers < Spree::BattReport::TopReport
  def name
    "Top Customers"
  end

  def description
    "Top purchasing customers, calculated by revenue"
  end

  def initialize params, limit
    super(params)

    orders.each do |order|
      user_email = order.user.try(:email) || order.try(:email)
      next unless user_email
      data[user_email] ||= {
        :email => user_email,
        :registered => order.user.present?? 'Yes' : 'No',
        :revenue => 0,
        :products => products_html(order),
        :units => 0
      }
      data[user_email][:revenue] += revenue(order)
      data[user_email][:units] += units(order)
    end

    self.ruportdata = Table(%w[email Registered Units Revenue Products])
    data.inject({}) { |h, (k, v) | h[k] = v[:revenue]; h }.sort { |a, b| a[1] <=> b [1] }.reverse[0..5].each do |k, v|
      ruportdata << {
        "email" => data[k][:email],
        "Registered" => data[k][:registered],
        "Units" => data[k][:units],
        "Revenue" => data[k][:revenue],
        "Products" => data[k][:products]
      }
    end
    ruportdata.replace_column("Revenue") { |r| "$%0.2f" % r.Revenue }
    ruportdata.rename_column("email", "Customer Email")
  end

  def products_html(order)
    "<div id='report-order-products'>" + order.products.map(&:name).join("<br />") + "</div>"
  end
end
