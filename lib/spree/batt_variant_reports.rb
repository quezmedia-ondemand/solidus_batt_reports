module Spree
  class BattVariantReports
    include Ruport
    attr_accessor :date_text, :ruportdata, :data, :params,
                  :unfiltered_params, :variants, :product_in_taxon,
                  :sku, :customer, :taxon, :product, :sku_text, :taxon_text,
                  :product_text

    def name
      "Base Everbatt Report"
    end

    def description
      "Base Everbatt Report"
    end

    def initialize params
      self.params = params
      self.data = {}
      self.ruportdata = {}
      self.unfiltered_params = params.clone

      self.product_in_taxon = true
      self.sku = false
      self.customer = false
      if params[:advanced_reporting]
        if params[:advanced_reporting][:taxon_id] && params[:advanced_reporting][:taxon_id] != ''
          self.taxon = Taxon.find(params[:advanced_reporting][:taxon_id])
        end
        if params[:advanced_reporting][:product_id] && params[:advanced_reporting][:product_id] != ''
          self.product = Product.find(params[:advanced_reporting][:product_id])
        end
        self.sku = params[:advanced_reporting][:sku]
        self.customer = params[:advanced_reporting][:customer_email]
      end

      if self.taxon && self.product && !self.product.taxons.include?(self.taxon)
        self.product_in_taxon = false
      end

      product_ids = []
      product_orders_ids, taxon_orders_ids, sku_orders_ids, customer_orders_ids = [], [], [], []

      if self.product.present?
        self.product_text = "Product: #{self.product.name}<br />"
        product_ids += self.product.id
      end

      if self.sku.present?
        self.sku_text = "Sku: #{self.sku}<br />"
        variant_ids = Spree::Variant.where("sku ~* ?", sku).pluck(:id)
        product_ids += Spree::Product.includes(:variants).where(spree_variants: { id: variant_ids }).pluck(:id)
      end

      if self.taxon
        self.taxon_text = "Taxon: #{self.taxon.name}<br />"
        product_ids += Spree::Product.includes(:taxons).where(spree_taxons: { id: taxon_id }).pluck(:id)
      end

      self.variants = product_ids.any?? Spree::Variant.includes(:product).where(spree_products: {id: product_ids}) : Spree::Variant.all

      # Below searchlogic date settings
      self.date_text = "Date Range:"
      if self.unfiltered_params
        if self.unfiltered_params[:completed_at_gt] != '' && self.unfiltered_params[:completed_at_lt] != ''
          self.date_text += " From #{self.unfiltered_params[:completed_at_gt]} to #{self.unfiltered_params[:completed_at_lt]}"
        elsif self.unfiltered_params[:completed_at_gt] != ''
          self.date_text += " After #{self.unfiltered_params[:completed_at_gt]}"
        elsif self.unfiltered_params[:completed_at_lt] != ''
          self.date_text += " Before #{self.unfiltered_params[:completed_at_lt]}"
        else
          self.date_text += " All"
        end
      else
        self.date_text += " All"
      end
    end
  end
end
